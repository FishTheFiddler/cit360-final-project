package Final360;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public abstract class BaseHandler implements HttpHandler {

    public String opponentNameResponseToJSON(OpponentNameResponse cResponse){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try {
            s = mapper.writeValueAsString(cResponse);
        } catch(JsonProcessingException a){
            System.err.println(a.toString());
        }
        return s;
    }

    public String getWordResponseToJSON(GetWordResponse cResponse){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try {
            s = mapper.writeValueAsString(cResponse);
        } catch(JsonProcessingException a){
            System.err.println(a.toString());
        }
        return s;
    }

    protected abstract void handleRequest(HttpExchange httpExchange, String playerName)  throws IOException;

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Map<String,String> params = BaseHandler.queryToMap(httpExchange.getRequestURI().getQuery());
        String playerName = params.get("name");
        this.handleRequest(httpExchange, playerName);
    }

    protected static Map<String, String> queryToMap(String query){
        Map<String, String> result = new HashMap<String, String>();
        for (String param : query.split("&")) {
            String pair[] = param.split("=");
            if (pair.length>1) {
                result.put(pair[0], pair[1]);
            }else{
                result.put(pair[0], "");
            }
        }
        return result;
    }

    protected void handleOpponentNameResponse(HttpExchange httpExchange, String opponent)  throws  IOException {
        handleOpponentNameErrorResponse(httpExchange, opponent, "");
}

    protected void handleOpponentNameErrorResponse(HttpExchange httpExchange, String opponent, String error_message)  throws  IOException {

        OutputStream outputStream = httpExchange.getResponseBody();

        OpponentNameResponse response = new OpponentNameResponse();

        response.setOpponent(opponent);
        response.setErrorMessage(error_message);

        String jsonResponse = opponentNameResponseToJSON(response);

        httpExchange.getResponseHeaders().set("Content-Type", "text/json");
        httpExchange.sendResponseHeaders(200, jsonResponse.length());
        outputStream.write(jsonResponse.getBytes());
        outputStream.flush();
        outputStream.close();

    }

    protected void handleGetWordResponse(HttpExchange httpExchange, String scrambled_word, String answer, String winner, String remaining_time)  throws  IOException {

        OutputStream outputStream = httpExchange.getResponseBody();

        GetWordResponse response = new GetWordResponse();

        response.setScrambledWord(scrambled_word);
        response.setAnswer(answer);
        response.setWinner(winner);
        response.setRemainingTime(remaining_time);

        String jsonResponse = getWordResponseToJSON(response);

        httpExchange.getResponseHeaders().set("Content-Type", "text/json");
        httpExchange.sendResponseHeaders(200, jsonResponse.length());
        outputStream.write(jsonResponse.getBytes());
        outputStream.flush();
        outputStream.close();

    }

}

