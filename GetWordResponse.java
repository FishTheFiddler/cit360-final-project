package Final360;

public class GetWordResponse {

    public String scrambled_word;
    public String answer;
    public String winner;
    public String remaining_time;



    // NAME FUNCTIONS
    public String getScrambledWord(){
        return scrambled_word;
    }
    public String setScrambledWord(String jname){
        this.scrambled_word = jname;
        return scrambled_word;
    }

    public String getAnswer(){
        return answer;
    }
    public String setAnswer(String jname){
        this.answer = jname;
        return answer;
    }

    public String getWinner(){
        return winner;
    }
    public String setWinner(String jname){
        this.winner = jname;
        return winner;
    }

    public String getRemainingTime(){
        return remaining_time;
    }
    public String setRemainingTime(String jname){
        this.remaining_time = jname;
        return remaining_time;
    }

    public String toString(){
        return "t";
    }

}
