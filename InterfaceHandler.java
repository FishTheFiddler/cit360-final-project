package Final360;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class InterfaceHandler  implements HttpHandler {

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        OutputStream outputStream = httpExchange.getResponseBody();
        String content = "";
        try {
            content = new String ( Files.readAllBytes( Paths.get("index.html") ) );
        } catch (IOException e) {
            e.printStackTrace();
        }
        httpExchange.getResponseHeaders().set("Content-Type", "text/html");
        httpExchange.sendResponseHeaders(200, content.length());
        outputStream.write(content.getBytes());
        outputStream.flush();
        outputStream.close();
    }





}
