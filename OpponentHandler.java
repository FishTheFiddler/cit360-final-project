package Final360;

import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;

public class OpponentHandler extends BaseHandler {

    public void handleRequest(HttpExchange httpExchange, String playerName)  throws IOException
    {
        handleOpponentNameResponse(httpExchange, Roster.getInstance().getOpponent(playerName));
    }

}

