package Final360;

import java.util.*;

public class Roster {

    private static Final360.Roster roster = null;
    public ArrayList<Team> playerTeams = null;
    public ArrayList<String> dictionary = null;
    public Map<String, Boolean> winnerWasRecognized = null;

    private Roster() {
        playerTeams = new ArrayList<Team>();
        winnerWasRecognized = new HashMap<String, Boolean>();
        dictionary = new ArrayList<String>();
        dictionary.add("bacon");
        dictionary.add("cheese");
        dictionary.add("pie");
        dictionary.add("lemon");
        dictionary.add("orange");
        dictionary.add("truck");
        dictionary.add("computer");
        dictionary.add("desk");
        dictionary.add("lamp");
        dictionary.add("action");
        dictionary.add("adventure");
        dictionary.add("book");
        dictionary.add("movie");
        dictionary.add("sister");
        dictionary.add("brother");
        dictionary.add("heaven");
        dictionary.add("dark");
        dictionary.add("advance");
        dictionary.add("pure");
        dictionary.add("encrypt");
    }

    public static Final360.Roster getInstance() {
        if (roster == null) {
            roster = new Final360.Roster();
        }
        return roster;
    }

    public String addPlayer(String playerName) {
        String opponent = "";
        for (int i = 0; i < this.playerTeams.size(); i++) {
            if (this.playerTeams.get(i).team.size() < 2) {
                this.playerTeams.get(i).team.add(playerName);
                opponent = this.playerTeams.get(i).team.get(0);
                this.playerTeams.get(i).wins.add(0);
                this.playerTeams.get(i).loses.add(0);
            }
        }
        if (opponent.equals("")) {
            this.playerTeams.add(new Team(playerName));
            this.playerTeams.get(this.playerTeams.size() - 1).wins.add(0);
            this.playerTeams.get(this.playerTeams.size() - 1).loses.add(0);
        }
        winnerWasRecognized.put(playerName, false);
        return opponent;
    }

    public boolean isNameTaken(String playerName) {
        for (int i = 0; i < this.playerTeams.size(); i++) {
            for (int j = 0; j < this.playerTeams.get(i).team.size(); j++) {
                if (this.playerTeams.get(i).team.get(j).equals(playerName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getOpponent(String playerName)
    {
        for (int i = 0; i < this.playerTeams.size(); i++) {
            for (int j = 0; j < this.playerTeams.get(i).team.size(); j++) {
                if (this.playerTeams.get(i).team.get(j).equals(playerName)) {
                    if (this.playerTeams.get(i).team.size() > 1) {
                        return this.playerTeams.get(i).team.get(1 - j);
                    } else {
                        return "";
                    }
                }
            }
        }
        return "";
    }

    public Team getTeam(String playerName) {
        for (int i = 0; i < this.playerTeams.size(); i++) {
            for (int j = 0; j < this.playerTeams.get(i).team.size(); j++) {
                if (this.playerTeams.get(i).team.get(j).equals(playerName)) {
                    return this.playerTeams.get(i);
                }
            }
        }
        return null;
    }
    public int getPlayerIndex(String playerName) {
        for (int i = 0; i < this.playerTeams.size(); i++) {
            for (int j = 0; j < this.playerTeams.get(i).team.size(); j++) {
                if (this.playerTeams.get(i).team.get(j).equals(playerName)) {
                    return j;
                }
            }
        }
        return -1;
    }

    public int getRemainingSeconds(String playerName) {
        return (int)(60 - ((System.nanoTime() - getTeam(playerName).startTime) / 1000000000));
    }

    private String getRandomWord() {
        return dictionary.get(getRandomNumber(0, dictionary.size() - 1));
    }

    private String scramble(String wordToScamble) {
        List<Character> characters = new ArrayList<Character>();
        for(char c:wordToScamble.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(wordToScamble.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public String getScrambledWord(String playerName){
        return getTeam(playerName).currentScrambledWord;
    }
    public String getAnswer(String playerName){
        return getTeam(playerName).currentWord;
    }
    public String getWinner(String playerName){
        return getTeam(playerName).winner;
    }
    public String getRemainingTime(String playerName){
        return String.valueOf(getRemainingSeconds(playerName));

    }

    public void update(String playerName) {
        if (getTeam(playerName).startTime == 0 || getRemainingSeconds(playerName) <= 0) {
            getTeam(playerName).startTime = System.nanoTime();
            getTeam(playerName).currentWord = getRandomWord();
            getTeam(playerName).currentScrambledWord = scramble(getTeam(playerName).currentWord);
        }
    }

}
