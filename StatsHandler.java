package Final360;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public class StatsHandler extends BaseHandler {

    public void handleRequest(HttpExchange httpExchange, String playerName)  throws IOException
    {
        OutputStream outputStream = httpExchange.getResponseBody();
        GetWordResponse response = new GetWordResponse();
        int i = Roster.getInstance().getPlayerIndex(playerName);
        String jsonResponse = "{\"wins\": " + Integer.toString(Roster.getInstance().getTeam(playerName).wins.get(i))
                + ", \"loses\": " + Integer.toString(Roster.getInstance().getTeam(playerName).loses.get(i)) + "}";
        httpExchange.getResponseHeaders().set("Content-Type", "text/json");
        httpExchange.sendResponseHeaders(200, jsonResponse.length());
        outputStream.write(jsonResponse.getBytes());
        outputStream.flush();
        outputStream.close();
    }

}

