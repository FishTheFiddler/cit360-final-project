package Final360;

import java.util.ArrayList;
import java.util.List;

public class Team {
    public ArrayList<String> team = new ArrayList<String>();
    public ArrayList<Integer> wins = new ArrayList<Integer>();
    public ArrayList<Integer> loses = new ArrayList<Integer>();
    public String currentWord = "";
    public String currentScrambledWord = "";
    public String winner = "";
    public long startTime = 0;
    public Team(String firstPlayer) {
        team.add(firstPlayer);
    }

}
